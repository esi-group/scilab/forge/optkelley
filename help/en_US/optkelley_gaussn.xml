<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from optkelley_gaussn.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="optkelley_gaussn" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 02-Jun-2010 $</pubdate>
  </info>

  <refnamediv>
    <refname>optkelley_gaussn</refname><refpurpose>Damped Gauss-Newton with Armijo rule.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [x,histout,costdata] = optkelley_gaussn ( x0 , f )
   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol )
   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol ,maxit )
   [x,histout,costdata] = optkelley_gaussn ( x0 , f , tol ,maxit , verbose )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x0:</term>
      <listitem><para> initial iterate</para></listitem></varlistentry>
   <varlistentry><term>f:</term>
      <listitem><para> r^T r/2 = objective function, the calling sequence for f should be [fout,gout,jac]=f(x) where fout=f(x) is a scalar, gout = jac^T r = grad f(x) is a COLUMN vector and jac = r' = Jacobian of r is an M x N matrix</para></listitem></varlistentry>
   <varlistentry><term>tol:</term>
      <listitem><para> (optional) termination criterion norm(grad) &lt; tol, default = 1.e-6. If tol==[], then the default is used.</para></listitem></varlistentry>
   <varlistentry><term>maxit:</term>
      <listitem><para> (optional) maximum iterations, default = 100. If maxit==[], then the default is used.</para></listitem></varlistentry>
   <varlistentry><term>verbose:</term>
      <listitem><para> (optional) if true, print messages (default=%f). If verbose==[], the default parameter is used.</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para> solution</para></listitem></varlistentry>
   <varlistentry><term>histout:</term>
      <listitem><para> iteration history Each row of histout is [norm(grad), f, number of step length reductions, iteration count]</para></listitem></varlistentry>
   <varlistentry><term>costdata:</term>
      <listitem><para> [num f, num grad, num hess] (for gaussn, num hess=0)</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Simple divide by 2 stepsize reduction.
This code comes with no guarantee or warranty of any kind.
   </para>
   <para>
At this stage all iteration parameters are hardwired in the code.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// A data fitting problem
M = 100;
N = 2;
// The function to approximate
// x: the input
// p: the parameter
// y: the output
function y = fapprox ( x , p )
y = sin(p(1)*x) + sin(p(2)*x)
endfunction
// The data to fit
rand("seed",0)
datax = linspace ( 0 , 4 , M )';
noisevar = 0.2;
noise = (2*rand ( M , 1 ) - 1 ) * noisevar;
//
exacty = fapprox ( datax , [2 3]);
datay = exacty + noise;
plot ( datax , exacty , "r-" );
plot ( datax , datay , "b*" );
legend ( [ "Exact" , "Data" ] );
// Returns the residual, a column vector of size M = 100.
// x: the parameter of the model
function r = residual ( p )
r = datay - fapprox ( datax , p )
endfunction
function [f, g, jac] = costfun ( x )
r = residual ( x )
f = 0.5 * r' * r
jac = numderivative ( residual , x )
g = jac' * r
endfunction
//
[f, g, jac] = costfun ( [2 3]' )
//
// Test with default parameters
x0 = [1.5 2]';
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
xopt
histout
costdata
niter = size(histout,"r")
fopt = histout(niter,2)

// Set tol
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, 0 );
// Use default tol and set maxit
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , 5 );
// Use default tol, maxit and set verbose
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , [] , %t );

// Other ways of using the default parameters
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] , [] );

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>C. T. Kelley, Dec 14, 1997</member>
   <member>Yann Collette, Scilab port</member>
   <member>Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments, added tests and example, improved management of input arguments</member>
   </simplelist>
</refsection>
</refentry>
