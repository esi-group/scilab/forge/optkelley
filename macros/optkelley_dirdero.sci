function z = optkelley_dirdero (x,w,f,gc,epsnew)
  // Finite difference directional derivative.
  //
  // Calling Sequence
  //   z = optkelley_dirdero(x,w,f,gc)
  //   z = optkelley_dirdero(x,w,f,gc,epsnew)
  //
  // Parameters
  //           x: current point
  //           w: direction
  //           f: function, the calling sequence is [fun,grad]=f(x)
  //           gc: current gradient, gc has usually been computed, before the call to optkelley_dirdero
  //           epsnew: difference increment (optional), default = 1.d-6
  //           z: directional derivative
  //
  // Description
  // Approximate f''(x) w.
  // This code comes with no guarantee or warranty of any kind.
  //
  // Examples
  // function [f,g] = rosenbrock ( x )
  //   f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  //   g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  //   g(2) = 200. * ( x(2) - x(1)**2 )
  // endfunction
  // function H = roseHess ( x )
  //   H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  //   H(1,2) = -400 * x(1)
  //   H(2,1) = H(1,2)
  //   H(2,2) = 200
  // endfunction
  // //
  // // Test with default epsnew
  // x0 = [-1.2 1.0]';
  // w = [1 0]';
  // [f0,g0] = rosenbrock ( x0 );
  // z = optkelley_dirdero(x0, w, rosenbrock, g0);
  // // Compare with the exact value
  // roseHess ( x0 ) * w
  // // Compare with optimal step (assuming f and f'' are of order unity)
  // z = optkelley_dirdero(x0, w, rosenbrock, g0, sqrt(%eps));
  //
  // Authors
  // C. T. Kelley, Dec 20, 1996
  // Yann Collette, Scilab port
  // Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments, added tests and example

  [nargout, nargin] = argn();
  if (nargin == 4) then
    epsnew = 1.d-6;
  end
  n = length(x);
  //
  // scale the step
  //
  if (norm(w) == 0) then
    error(mprintf("%s: The direction w has a zero norm.","optkelley_dirdero"));
  end
  epsnew = epsnew/norm(w);
  //
  // del and g1 could share the same space if storage
  // is more important than clarity
  //
  del     = x+epsnew*w;
  [f1,g1] = f(del);
  z       = (g1 - gc)/epsnew;
endfunction

