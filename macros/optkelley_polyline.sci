function [xp, idid, lambda] = optkelley_polyline (xc, fc, gc, d, ft, f, maxarm)
// Polynomial line search.
//
// Calling Sequence
//   [xp, idid, lambda]=optkelley_polyline(xc, fc, gc, d, ft, fobj, maxarm)
//
// Parameters
//        xc: current point
//        fc: current function value
//        gc: current gradient value
//         d: direction
//        ft: trial function (rejected value)
//         f: objective function. The calling sequence for f should be [fout,gout]=f(x) where fout=f(x) is a scalar and gout = grad f(x) is a COLUMN vector
//    maxarm: maximum number of step length reductions
//        xp: successful new point (if it exists)
//       idid: number of calls to f (if line search succeeds) or -1 if line search fails.
//     lambda: the step size which satisfies the sufficient decrease condition
//
// Description
// Call after first point is rejected.
// Returns the point xp such that the sufficient decrease condition is satifies.
// The sufficient decrease condition writes f(xc+lambda*d) <= fc+alp*lambda*gc'*d
// with alp = 1.d-4.
// This code comes with no guarantee or warranty of any kind.
//
// Requires: optkelley_polymod.sci
//
// Authors
// C. T. Kelley, Dec 29, 1997
// Yann Collette, Scilab port
// Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments

//
// line search parameters that everyone uses
//

alp   = 1.d-4;
blow  = .1;
bhigh = .5;

//
// Set up the search
//

q0    = fc;
qp0   = gc'*d;
qc    = ft;
lamc  = 1;
iarm  = 0;
numf  = 0;
fgoal = fc+alp*lamc*qp0;

while (ft > fgoal)
  iarm = iarm+1;
  if (iarm==1) then  // quadratic
    lambda = optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh);
  else
    lambda = optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh, lamm, qm);
  end
  qm   = qc;
  lamm = lamc;
  lamc = lambda;
  xt   = xc+lambda*d;
  ft   = f(xt);
  numf = numf+1;
  qc   = ft;
  if (iarm > maxarm) then
    mprintf("%s: line search failure\n","optkelley_polyline");
    idid = -1;
    xp   = xc;
    return;
  end
  fgoal = fc+alp*lamc*qp0;
end
xp   = xt;
idid = numf;
endfunction
