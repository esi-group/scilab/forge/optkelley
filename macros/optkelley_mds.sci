function [x,lhist,histout] = optkelley_mds(x0,f,tol,maxit,budget)
// Multidirectional search.
//
// Calling Sequence
//   [x,lhist,histout] = optkelley_mds(x0,f,tol,maxit,budget)
//
// Parameters
//    x0: vertices of initial simplex =  (n x n+1 matrix). The code will order the vertices for you and no benefit is accrued if you do it yourself.
//    f: objective function
//    tol: absolute termination tolerance on function. The criteria is | best f-value - worst f-value | < tol, where the best and worst function values are associated with the current simplex.
//    maxit: maximum number of iterations  (default = 100).
//    budget: max f evals (default=50*number of variables). The iteration will terminate after the iteration that exhausts the budget.
//    x: final simplex (n x n+1) matrix
//    lhist: number of iterations before termination
//    histout: iteration history, updated after each nonlinear iteration, with size lhist x 4 array. The rows are: [fcount, fval, norm(grad), dist, diam].
//    fcount: cumulative function evals,
//    fval: current best function value,
//    dist: difference between worst and best function values,
//    diam: max oriented length
//
// Description
// This is a direct search method i.e. it does not use the gradient of the objective function.
// This code comes with no guarantee or warranty of any kind.
//
// Examples
// function y = quad ( x )
//   x1 = [1 1]'
//   x2 = [2 2]'
//   y = max ( norm(x-x1) , norm(x-x2) )
// endfunction
// x0 = [-1.2 1]';
// v1 = x0 + [0.1 0]';
// v2 = x0 + [0 0.1]';
// v0 = [x0 v1 v2];
// [x,lhist,histout] = optkelley_mds(v0,quad,1.e-4,100,100);
// xopt = x(:,1)
// fopt = histout(lhist,2)
//
// Authors
// C. T. Kelley, July 17, 1998
// Yann Collette, Scilab port
// Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments
//

//
[nargout, nargin] = argn();
//
// initialize counters
//
lhist  = 0;
fcount = 0;
//
// set debug=1 to print out iteration stats
//
_debug = 0;
//
// Set the MDS parameters
//
de = 2;
dc = .5;
//
// cache control paramters
//
global cache_size cache cache_ptr cache_fvals
[n,m]       = size(x0);
cache_size  = 4*n;
cache       = zeros(n,cache_size);
cache_ptr   = 0;
cache_fvals = zeros(cache_size,1);
//
if (nargin < 4) then
  maxit=100;
end
if (nargin < 5) then
  budget=100*n;
end
//
// Order the vertices for the first time
//
x       = x0;
histout = [];
itout   = 0;
_orth   = 0;
fv      = zeros(n+1,1);
xtmp    = zeros(n,n+1);
z       = zeros(n,n);
delf    = zeros(n,1);
for j=1:n+1
  fv(j) = geval(f,x(:,j));
end
fcount  = fcount+n+1;
[fs,is] = mtlb_sort(fv);
xtmp    = x(:,is);
x       = xtmp;
fv      = fs;
itc     = 0;
dist    = fv(n+1)-fv(1);
diam    = zeros(n,1);
for j=2:n+1
  v(:,j-1)  = -x(:,1)+x(:,j);
  diam(j-1) = norm(v(:,j-1));
end
lhist   = lhist+1;
thist   = [fcount, fv(1), dist, max(diam)];
histout = [histout',thist']';
//
// main MDS loop
//
while((itc < maxit) & (dist > tol) & (fcount <= budget))
  happy = 0;
  //
  //  reflect
  //
  for j=2:n+1
    xr(:,j)     = x(:,1) - v(:,j-1);
    [fr(j),ctr] = geval(f,xr(:,j));
    fcount      = fcount+ctr;
  end
  //fcount=fcount+n;
  fvr = min(fr(2:n+1));
  if (fv(1) > fvr) then
    happy = 1;
    //
    // expand
    //
    for j=2:n+1
      xe(:,j) = x(:,1) - de*v(:,j-1);
      fe(j)   = f(xe(:,j));
    end
    fcount = fcount+n;
    fve    = min(fe(2:n+1));
    if (fvr > fve) then
      for j=2:n+1
        x(:,j) = xe(:,j);
        fv(j)  = fe(j);
      end
    else
      for j=2:n+1
        x(:,j) = xr(:,j);
        fv(j)  = fr(j);
      end
    end
  end
  //
  // contract
  //
  if (happy==0) then
    for j=2:n+1
      x(:,j)      = x(:,1) + dc*v(:,j-1);
      [fv(j),ctr] = geval(f,x(:,j));
      fcount      = fcount+ctr;
    end
    //   fcount=fcount+n;
  end
  //
  //  sort the new vertices
  //
  [fs,is] = mtlb_sort(fv);
  xtmp=x(:,is);
  x=xtmp;
  fv=fs;
  //
  //  compute the diameter of the new simplex and the iteration data
  //
  for j=2:n+1
    v(:,j-1)  = -x(:,1)+x(:,j);
    diam(j-1) = norm(v(:,j-1));
  end
  dist    = fv(n+1)-fv(1);
  lhist   = lhist+1;
  thist   = [fcount, fv(1), dist, max(diam)];
  histout = [histout',thist']';
end
endfunction
//
//
//
function [fs,ctr] = geval(fh,xh)
global cache_size cache cache_ptr cache_fvals
for i=1:cache_size
  nz(i) = norm(xh-cache(:,i));
end
[vz,iz] = min(nz);
if ((vz == 0) & (cache_ptr ~=0)) then
  fs  = cache_fvals(iz);
  ctr = 0;
else
    fs  = fh(xh);
    ctr = 1;
    cache_ptr = modulo(cache_ptr,cache_size)+1;
    cache(:,cache_ptr)     = xh;
    cache_fvals(cache_ptr) = fs;
end
endfunction

