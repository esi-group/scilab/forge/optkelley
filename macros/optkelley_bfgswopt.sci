function [x,histout,costdata] = optkelley_bfgswopt (varargin)
  // Steepest descent/bfgs with polynomial line search.
  //
  // Calling Sequence
  //   x = optkelley_bfgswopt(x0,f)
  //   x = optkelley_bfgswopt(x0,f,tol)
  //   x = optkelley_bfgswopt(x0,f,tol,maxit)
  //   x = optkelley_bfgswopt(x0,f,tol,maxit,hess0)
  //   x = optkelley_bfgswopt(x0,f,tol,maxit,hess0,verbose)
  //   [x,histout,costdata] = optkelley_bfgswopt(...)
  //
  // Parameters
  // x0: initial iterate
  // f: objective function, the calling sequence for f should be [fout,gout]=f(x) where fout=f(x) is a scalar and gout = grad f(x) is a COLUMN vector
  // tol: (optional) termination criterion norm(grad) < tol (default = 1.d-6). If tol==[], the default parameter is used.
  // maxit: (optional) maximum iterations (default = 20). If maxit==[], the default parameter is used.
  // hess0: (optional), function that computes the action of the initial inverse Hessian on a vector. The default is H_0 = I (ie no action). The format of hess0 is h0v = hess0(v) is the action of H_0^{-1} on a vector v. If hess0==[], the default parameter is used.
  // verbose: (optional), if true, print messages (default=%f). If verbose==[], the default parameter is used.
  // x: solution
  // histout: iteration history. Each row of histout is [norm(grad), f, num step reductions, iteration count]
  // costdata: [num f, num grad, num hess]  (num hess=0 always, in here for compatibility with optkelley_steep.sci)
  //
  // Description
  // This code comes with no guarantee or warranty of any kind.
  //
  // If the BFGS update succeeds, backtrack on that with a polynomial line search, otherwise we use Steepest Descent.
  // Uses Steve Wright storage of H^-1.
  //
  // Examples
  // function [f,g] = rosenbrock ( x )
  //   f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  //   g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  //   g(2) = 200. * ( x(2) - x(1)**2 )
  // endfunction
  // x0 = [-1.2 1.0]';
  // maxit     = 1000;
  // tol       = 1e-6;
  // [xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,tol,maxit);
  // xopt
  // histout
  // costdata
  // niter = size(histout,"r");
  // fopt = histout(niter,2)
  //
  // // Set maxit, let tol to the default.
  // [xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000);
  //
  // // Set maxit, set verbose, let other parameters to the default.
  // [xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000,[],%t);
  //
  // // Provide the effect of the inverse Hessian
  // function h0v = roseHess0 ( v )
  //   x = [-1.2 1.0]'
  //   H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  //   H(1,2) = -400 * x(1)
  //   H(2,1) = H(1,2)
  //   H(2,2) = 200
  //   h0v = inv(H) * v
  // endfunction
  // [xopt,histout,costdata] = optkelley_bfgswopt(x0,rosenbrock,[],1000,roseHess0);
  //
  // Authors
  // C. T. Kelley, July 17, 1997
  // Yann Collette, Scilab port
  // Michael Baudin, DIGITEO, 2010, Update to Scilab 5.2, formatted comments, added tests and example, improved management of input arguments

  //
  //
  // At this stage all iteration parameters are hardwired in the code.
  //
  [lhs, rhs] = argn();
  if ( rhs < 2 | rhs > 6 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 2 to 6 are expected."), "optkelley_bfgswopt", rhs);
    error(errmsg)
  end
  //
  x0 = varargin(1)
  f = varargin(2)
  if (rhs < 3) then
    tol = 1.d-6;
  else
    tol = varargin(3)
    if ( tol == [] ) then
      tol = 1.d-6
    end
  end
  if (rhs < 4) then
    maxit = 20;
  else
    maxit = varargin(4)
    if ( maxit == [] ) then
      maxit = 20
    end
  end
  // userhess = 1 if the user provided the Hessian matrix
  if (rhs < 5) then
    userhess = 0;
  else
    hess0 = varargin(5)
    if ( hess0 == [] ) then
      userhess = 0;
    else
      userhess = 1;
    end
  end
  if (rhs < 6) then
    verbose = %f;
  else
    verbose = varargin(6)
    if ( verbose == [] ) then
      verbose = %f
    end
  end
  //
  _debug = 0;
  blow   = .1;
  bhigh  = .5;
  numf   = 0;
  numg   = 0;
  numh   = 0;
  itc    = 1;
  xc     = x0;
  maxarm = 10;
  nsmax  = 50;
  _debug = 0;
  //
  n        = length(x0);
  [fc,gc] = f(xc);
  numf = numf+1;
  numg = numg+1;
  ithist = zeros(maxit,3);
  ithist(1,1) = norm(gc);
  ithist(1,2) = fc;
  ithist(1,4) = itc-1;
  ithist(1,3) = 0;
  if (_debug==1) then
    ithist(itc,:)
  end
  go     = zeros(n,1);
  alpha  = zeros(nsmax,1);
  _beta  = alpha;
  sstore = zeros(n,nsmax);
  ns     = 0;
  //
  //	dsdp = - H_c^{-1} grad_+ if ns > 0
  //
  while((norm(gc) > tol) & (itc <= maxit))
    dsd  = -gc;
    dsdp = -gc;
    if (userhess==1) then
      dsdp = hess0(dsd);
    end
    if (ns>1) then
      if (userhess==1) then
        dsdp = bfgsw(sstore,alpha,_beta,ns,dsd,hess0);
      else
        dsdp = bfgsw(sstore,alpha,_beta,ns,dsd);
      end
    end
    //
    //
    // compute the direction
    //
    if (ns==0) then
      dsd = -gc;
      if (userhess==1) then
        dsd = hess0(dsd)
      end
    else
      xi   = -dsdp;
      b0   = -1/(y'*s);
      zeta = (1-1/lambda)*s+xi;
      a1   = b0*b0*(zeta'*y);
      a1   = -b0*(1 - 1/lambda)+b0*b0*y'*xi;
      a    = -(a1*s+b0*xi)'*gc;
      //
      //		We save go=s'*g_old just so we can use it here
      //		and avoid some cancellation error
      //
      alphatmp = a1+2*a/go;
      b = -b0*go;
      //
      //
      dsd=a*s+b*xi;
    end
    //
    //
    //
    if (dsd'*gc > -1.d-6*norm(dsd)*norm(gc)) then
      if ( verbose ) then
        mprintf("%s: loss of descent\n","optkelley_bfgswopt")
      end
      [itc, dsd'*gc]
      dsd = -gc;
      ns  = 0;
    end
    lambda = 1;
    //
    //       fixup against insanely long steps see (3.50) in the book
    //
    lambda = min(1,100/(1 + norm(gc)));
    xt     = xc+lambda*dsd;
    ft     = f(xt);
    numf   = numf+1;
    itc    = itc+1;
    old    = 1;
    if (old==0) then
      goal = fc+1.d-4*(gc'*dsd);
      iarm = 0;
      if (ft > goal) then
        [xt,iarm,lambda]=optkelley_polyline(xc,fc,gc,dsd,ft,f,maxarm);
        if (iarm==-1) then
          x       = xc;
          histout = ithist(1:itc,:);
          if ( verbose ) then
            mprintf("%s: line search failure\n","optkelley_bfgswopt");
          end
          return;
        end
      end
    end
    if (old==1) then
      iarm    = 0;
      goalval = .0001*(dsd'*gc);
      q0      = fc;
      qp0     = gc'*dsd;
      lamc    = lambda; qc=ft;
      while(ft > fc + lambda*goalval )
        iarm = iarm+1;
        if (iarm==1) then
          lambda = optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh);
        else
          lambda = optkelley_polymod(q0, qp0, lamc, qc, blow, bhigh, lamm, qm);
        end
        qm   = qc;
        lamm = lamc;
        lamc = lambda;
        xt   = xc+lambda*dsd;
        ft   = f(xt);
        qc   = ft;
        numf = numf+1;
        if (iarm > maxarm) then
          x = xc;
          histout = ithist(1:itc,:);
          if ( verbose ) then
            mprintf("%s: too many backtracks in BFGS line search\n","optkelley_bfgswopt");
          end
          return;
        end
      end
    end
    s  = xt-xc;
    y  = gc;
    go = s'*gc;
    xc = xt;
    [fc,gc] = f(xc);
    y       = gc-y;
    numf    = numf+1;
    numg    = numg+1;
    //
    //   restart if y'*s is not positive or we're out of room
    //
    if ((y'*s <= 0) | (ns==nsmax)) then
      if ( verbose ) then
        mprintf("%s: loss of positivity or storage\n","optkelley_bfgswopt");
      end
      [ns, y'*s]
      ns = 0;
    else
      ns = ns+1;
      sstore(:,ns) = s;
      if (ns>1) then
        alpha(ns-1) = alphatmp;
        _beta(ns-1) = b0/(b*lambda);
      end
    end
    ithist(itc,1) = norm(gc);
    ithist(itc,2) = fc;
    ithist(itc,4) = itc-1;
    ithist(itc,3) = iarm;
    if (_debug==1) then
      ithist(itc,:)
    end
  end
  x        = xc;
  histout  = ithist(1:itc,:);
  costdata = [numf, numg, numh];
endfunction
//
// bfgsw
//
// C. T. Kelley, Dec 20, 1996
//
// This code comes with no guarantee or warranty of any kind.
//
// This code is used in optkelley_bfgswopt.sci
//
// There is no reason to ever call this directly.
//
// form the product of the bfgs approximate inverse Hessian
// with a vector using the Steve Wright method
//
function dnewt=bfgsw(sstore,alpha,_beta,ns,dsd,hess0)
  [lhs, rhs] = argn();
  userhess = 0;
  if (rhs==6) then
    userhess=1;
  end
  dnewt = dsd;
  if (userhess==1) then
    dnewt=hess0(dnewt);
  end
  if (ns<=1) then
    return;
  end;
  dnewt = dsd;
  n     = length(dsd);
  if (userhess==1) then
    dnewt=hess0(dnewt);
  end
  sigma  = sstore(:,1:ns-1)'*dsd;
  gamma1 = alpha(1:ns-1).*sigma;
  gamma2 = _beta(1:ns-1).*sigma;
  gamma3 = gamma1+_beta(1:ns-1).*(sstore(:,2:ns)'*dsd);
  delta  = gamma2(1:ns-2)+gamma3(2:ns-1);
  dnewt  = dnewt+gamma3(1)*sstore(:,1)+gamma2(ns-1)*sstore(:,ns);
  if(ns <=2) then
    return;
  end
  dnewt = dnewt+sstore(1:n,2:ns-1)*delta(1:ns-2);
endfunction

