mode(1)
//
// Demo of optkelley_mds.sci
//

function y = quad ( x )
x1 = [1 1]'
x2 = [2 2]'
y = max ( norm(x-x1) , norm(x-x2) )
endfunction
x0 = [-1.2 1]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v0 = [x0 v1 v2];
[x,lhist,histout] = optkelley_mds(v0,quad,1.e-4,100,100);
xopt = x(:,1)
fopt = histout(lhist,2)
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_mds.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
