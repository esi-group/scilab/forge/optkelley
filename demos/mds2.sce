// Copyright (C) 2010 - DIGITEO - Michael Baudin
function y = quad ( x )
  x1 = [1 1]'
  x2 = [2 2]'
  y = max ( norm(x-x1) , norm(x-x2) )
  plot ( x(1),x(2),"bo")
endfunction
function y = quadC ( x1 , x2 )
  x = [x1 x2]'
  x1 = [1 1]'
  x2 = [2 2]'
  y = max ( norm(x-x1) , norm(x-x2) )
endfunction

mprintf("Please wait...\n")
f = scf(100001);


n = 60;
x = linspace ( -2, 3 , n );
y = linspace ( -2, 3 , n );
contour ( x , y , quadC ,  [0.7 0.9 1.1 1.4 2 3 4] )

x0 = [0 0]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v = [x0 v1 v2];
[x,lhist,histout] = optkelley_mds(v,quad,1.e-4,100,100)

//
// Load this script into the editor
//
filename = 'mds2.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

