// Copyright (C) - Yann Collette
// Copyright (C) 2010 - DIGITEO - Michael Baudin

function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
  if ( x(1) < 2 & x(1) > -2 & x(2) < 2 & x(2) > -2 ) then
    plot ( x(1) , x(2) , "bo" )
  end
endfunction
function y = rosenbrockC ( x1 , x2 )
  x = [x1 x2]
  y = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction

mprintf("Please wait...\n")
f = scf(100001);

x = linspace ( -2, 2 , 100 );
y = linspace ( -2, 2 , 100 );
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

x0 = [-1.2 1.0]';


contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

plot(x0(1), x0(2), "ro");

[x_opt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 , 1000,  1.d-12);

plot(x_opt(1), x_opt(2), "go");

//
// Load this script into the editor
//
filename = 'ntrust.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

