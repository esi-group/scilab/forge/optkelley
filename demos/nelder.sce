// Copyright (C) 2010 - DIGITEO - Michael Baudin
function y = rosenbrock ( x )
  y = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  plot ( x(1),x(2),"bo")
endfunction
function y = rosenbrockC ( x1 , x2 )
  x = [x1 x2]
  y = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
endfunction

mprintf("Please wait...\n")
f = scf(100001);

x = linspace ( -2, 2 , 100 );
y = linspace ( -2, 2 , 100 );
contour ( x , y , rosenbrockC ,  [1 10 100 500 1000] )

x0 = [-1.2 1.0]';
v1 = x0 + [0.1 0]';
v2 = x0 + [0 0.1]';
v = [x0 v1 v2];
[x,lhist,histout,simpdata]=optkelley_nelder(v,rosenbrock,1.e-4,500,500);

//
// Load this script into the editor
//
filename = 'nelder.sce';
dname = get_absolute_file_path(filename);
editor ( dname + filename );

