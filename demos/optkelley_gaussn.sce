mode(1)
//
// Demo of optkelley_gaussn.sci
//

// A data fitting problem
M = 100;
N = 2;
// The function to approximate
// x: the input
// p: the parameter
// y: the output
function y = fapprox ( x , p )
y = sin(p(1)*x) + sin(p(2)*x)
endfunction
// The data to fit
rand("seed",0)
datax = linspace ( 0 , 4 , M )';
noisevar = 0.2;
noise = (2*rand ( M , 1 ) - 1 ) * noisevar;
//
exacty = fapprox ( datax , [2 3]);
datay = exacty + noise;
plot ( datax , exacty , "r-" );
plot ( datax , datay , "b*" );
legend ( [ "Exact" , "Data" ] );
// Returns the residual, a column vector of size M = 100.
// x: the parameter of the model
function r = residual ( p )
r = datay - fapprox ( datax , p )
endfunction
function [f, g, jac] = costfun ( x )
r = residual ( x )
f = 0.5 * r' * r
jac = numderivative ( residual , x )
g = jac' * r
endfunction
//
[f, g, jac] = costfun ( [2 3]' )
//
// Test with default parameters
x0 = [1.5 2]';
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
xopt
histout
costdata
niter = size(histout,"r")
fopt = histout(niter,2)
halt()   // Press return to continue

// Set tol
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, 0 );
// Use default tol and set maxit
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , 5 );
// Use default tol, maxit and set verbose
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun, [] , [] , %t );
halt()   // Press return to continue

// Other ways of using the default parameters
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] );
[xopt, histout, costdata] = optkelley_gaussn ( x0, costfun , [] , [] , [] );
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_gaussn.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
