mode(1)
//
// Demo of optkelley_ntrust.sci
//

function [f,g] = rosenbrock ( x )
f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 );
lhist = size(histout,"r");
fopt = histout(lhist,2)
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "optkelley_ntrust.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );
