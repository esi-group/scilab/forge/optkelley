// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction


function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction

//
// Returns H
//
function H = roseHess ( x )
  H(1,1) = 1200 * x(1)**2 - 400 * x(2) + 2
  H(1,2) = -400 * x(1)
  H(2,1) = H(1,2)
  H(2,2) = 200
endfunction

// Test with default epsnew
x0 = [-1.2 1.0]';
w = [1 0]';
[f0,g0] = rosenbrock ( x0 );
computed = optkelley_dirdero(x0, w,rosenbrock,g0);
H0 = roseHess ( x0 );
expected = H0 * w;
assert_close ( computed, expected, 1.e-5 );

// Test with customized epsnew
x0 = [-1.2 1.0]';
w = [1 0]';
[f0,g0] = rosenbrock ( x0 );
computed = optkelley_dirdero(x0, w,rosenbrock,g0,sqrt(%eps));
H0 = roseHess ( x0 );
expected = H0 * w;
assert_close ( computed, expected, 1.e-7 );

