// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction


function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction

// Test with customized parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 , 1000,  1.d-12);
assert_close ( xopt , [1.0   1.0]', 1e-11 );
lhist = size(histout,"r");
fopt = histout(lhist,2);
assert_close ( fopt , 0.0 , 1.e-20 );



// Test with default resolution
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 , 1000);
assert_close ( xopt , [1.0   1.0]', 1e-11 );
lhist = size(histout,"r");
fopt = histout(lhist,2);
assert_close ( fopt , 0.0 , 1.e-20 );


// Test with default resolution + default maxit
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_ntrust(x0, rosenbrock, 1.e-8 );
assert_close ( xopt , [1.0   1.0]', 1e-11 );
lhist = size(histout,"r");
fopt = histout(lhist,2);
assert_close ( fopt , 0.0 , 1.e-20 );


