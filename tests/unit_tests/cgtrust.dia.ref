// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction
// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );
// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );
// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );
// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );
// Set tol
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, 0);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_equal ( xopt , [1.   1.]' );
assert_equal ( fopt , 0 );
assert_equal ( niter , 28 );
// Set tol and eta
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.5]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 38 );
// Set tol, eta and maxit
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 22]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-3 );
assert_close ( fopt , 0 , 1.e-4 );
assert_equal ( niter , 22 );
// Set tol, eta, maxit and maxitl
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 50 1]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e1 );
assert_equal ( fopt < 5 , %t );
assert_equal ( niter , 50 );
// Use default params and set resolution
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], 1e-12);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
// Use default params and resolution, set verbose
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], [] , %t );
it=1, g=    2.32868e+02, f=    2.42000e+01, trrad=    1.56205e+00, itsl=0
it=2, g=    3.09448e+01, f=    4.56778e+00, trrad=    1.56205e+00, itsl=1
it=3, g=    1.94888e+00, f=    4.12838e+00, trrad=    1.56205e+00, itsl=1
optkelley_cgtrust: negative curvature
it=4, g=    8.83751e+00, f=    3.46473e+00, trrad=    3.90512e-01, itsl=2
it=5, g=    1.48207e+01, f=    2.86142e+00, trrad=    7.81025e-01, itsl=2
it=6, g=    1.27010e+01, f=    2.26545e+00, trrad=    7.81025e-01, itsl=2
it=7, g=    7.87409e+00, f=    1.73432e+00, trrad=    7.81025e-01, itsl=2
it=8, g=    8.81708e+00, f=    1.34597e+00, trrad=    7.81025e-01, itsl=2
it=9, g=    3.37877e+00, f=    9.57644e-01, trrad=    7.81025e-01, itsl=2
it=10, g=    2.57506e+00, f=    7.28600e-01, trrad=    1.25323e-01, itsl=2
it=11, g=    2.25126e+00, f=    5.45533e-01, trrad=    2.50646e-01, itsl=2
it=12, g=    1.22661e+01, f=    4.80696e-01, trrad=    2.50646e-01, itsl=2
it=13, g=    8.41178e-01, f=    2.87227e-01, trrad=    2.50646e-01, itsl=1
it=14, g=    9.49964e+00, f=    2.19883e-01, trrad=    2.50646e-01, itsl=2
it=15, g=    4.82350e-01, f=    1.35468e-01, trrad=    2.50646e-01, itsl=1
it=16, g=    8.13865e+00, f=    9.75101e-02, trrad=    2.50646e-01, itsl=2
it=17, g=    2.60376e-01, f=    4.99322e-02, trrad=    2.50646e-01, itsl=1
it=18, g=    7.36678e+00, f=    4.04452e-02, trrad=    2.50646e-01, itsl=2
it=19, g=    1.13918e-01, f=    9.14292e-03, trrad=    2.50646e-01, itsl=1
it=20, g=    3.24586e+00, f=    5.50166e-03, trrad=    2.50646e-01, itsl=2
it=21, g=    1.61115e-02, f=    1.59984e-04, trrad=    2.50646e-01, itsl=1
it=22, g=    6.88025e-02, f=    2.42738e-06, trrad=    2.50646e-01, itsl=2
it=23, g=    2.25343e-04, f=    6.35275e-08, trrad=    2.50646e-01, itsl=1
it=24, g=    2.81870e-05, f=    4.02383e-13, trrad=    2.50646e-01, itsl=2
it=25, g=    6.78383e-08, f=    5.76170e-15, trrad=    2.50646e-01, itsl=1
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
// Configure all parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 1000 100], 1e-12 , %t );
it=1, g=    2.32868e+02, f=    2.42000e+01, trrad=    1.56205e+00, itsl=0
it=2, g=    3.09448e+01, f=    4.56778e+00, trrad=    1.56205e+00, itsl=1
it=3, g=    1.94888e+00, f=    4.12838e+00, trrad=    1.56205e+00, itsl=1
optkelley_cgtrust: negative curvature
it=4, g=    8.83751e+00, f=    3.46473e+00, trrad=    3.90512e-01, itsl=2
it=5, g=    1.48207e+01, f=    2.86142e+00, trrad=    7.81025e-01, itsl=2
it=6, g=    1.27010e+01, f=    2.26545e+00, trrad=    7.81025e-01, itsl=2
it=7, g=    7.87409e+00, f=    1.73432e+00, trrad=    7.81025e-01, itsl=2
it=8, g=    8.81708e+00, f=    1.34597e+00, trrad=    7.81025e-01, itsl=2
it=9, g=    3.37877e+00, f=    9.57644e-01, trrad=    7.81025e-01, itsl=2
it=10, g=    2.57506e+00, f=    7.28600e-01, trrad=    1.25323e-01, itsl=2
it=11, g=    2.25126e+00, f=    5.45533e-01, trrad=    2.50646e-01, itsl=2
it=12, g=    1.22661e+01, f=    4.80696e-01, trrad=    2.50646e-01, itsl=2
it=13, g=    8.41178e-01, f=    2.87227e-01, trrad=    2.50646e-01, itsl=1
it=14, g=    9.49964e+00, f=    2.19883e-01, trrad=    2.50646e-01, itsl=2
it=15, g=    4.82350e-01, f=    1.35468e-01, trrad=    2.50646e-01, itsl=1
it=16, g=    8.13865e+00, f=    9.75101e-02, trrad=    2.50646e-01, itsl=2
it=17, g=    2.60376e-01, f=    4.99322e-02, trrad=    2.50646e-01, itsl=1
it=18, g=    7.36678e+00, f=    4.04452e-02, trrad=    2.50646e-01, itsl=2
it=19, g=    1.13918e-01, f=    9.14292e-03, trrad=    2.50646e-01, itsl=1
it=20, g=    3.24586e+00, f=    5.50166e-03, trrad=    2.50646e-01, itsl=2
it=21, g=    1.61115e-02, f=    1.59984e-04, trrad=    2.50646e-01, itsl=1
it=22, g=    6.88025e-02, f=    2.42738e-06, trrad=    2.50646e-01, itsl=2
it=23, g=    2.25343e-04, f=    6.35275e-08, trrad=    2.50646e-01, itsl=1
it=24, g=    2.81870e-05, f=    4.02383e-13, trrad=    2.50646e-01, itsl=2
it=25, g=    6.78383e-08, f=    5.76170e-15, trrad=    2.50646e-01, itsl=1
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
