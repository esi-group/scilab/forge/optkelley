// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction


function [f,g] = rosenbrock ( x )
  f = 100.0 *(x(2)-x(1)^2)^2 + (1-x(1))^2;
  g(1) = - 400. * ( x(2) - x(1)**2 ) * x(1) -2. * ( 1. - x(1) )
  g(2) = 200. * ( x(2) - x(1)**2 )
endfunction

// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );

// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );

// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );

// Test with default parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock , [] , [] , [] );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );
assert_equal ( size(histout,"c") , 4 );
assert_equal ( costdata , [91 69] );

// Set tol
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, 0);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_equal ( xopt , [1.   1.]' );
assert_equal ( fopt , 0 );
assert_equal ( niter , 28 );

// Set tol and eta
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.5]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 38 );

// Set tol, eta and maxit
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 22]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-3 );
assert_close ( fopt , 0 , 1.e-4 );
assert_equal ( niter , 22 );

// Set tol, eta, maxit and maxitl
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 50 1]);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e1 );
assert_equal ( fopt < 5 , %t );
assert_equal ( niter , 50 );

// Use default params and set resolution
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], 1e-12);
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );

// Use default params and resolution, set verbose
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [], [] , %t );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );

// Configure all parameters
x0 = [-1.2 1.0]';
[xopt, histout, costdata] = optkelley_cgtrust(x0, rosenbrock, [1e-6 0.1 1000 100], 1e-12 , %t );
niter = size(histout,"r");
fopt = histout(niter,2);
assert_close ( xopt , [1.   1.]', 1.e-6 );
assert_close ( fopt , 0 , 1.e-14 );
assert_equal ( niter , 25 );

